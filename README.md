# Like a Cat out of Hell!

2D Pixel Art Puzzle Platform game made by Xecestel (Celeste Privitera) as a submission for the 9th edition of the Godot Wild Jam (https://itch.io/jam/godot-wild-jam-9).
In this game you play as Lilith, a black cat who just recently passed away. She is now stuck in Purgatory and in order to enter Heaven she has to open the Pearly Gates by retrieving the Heavens' Keys.
Jump, walk, run and solve puzzles in order to clear levels and get the keys. And just by pressing a key you can jump between Heaven and Hell like a bat, or rather like a cat out of Hell!

[*Play this game on itch.io!*](https://xecestel.itch.io/like-a-cat-out-of-hell)

# Licenses

Like a Cat out of Hell
Copyright (C) 2019  Celeste Privitera

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.


The graphic assets made by Xecestel (Celeste Privitera) are licensed under the Creative Commons Attribution-ShareAlike 4.0 Unported License.
To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/3.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
or read "LICENSE_CC-BY-SA.txt" in this directory.

Said assets are:
- "sprites" directory content
- The heaven part of "TileSet.png"

Third parties assets are licensed under licenses chose by their author, as listed:

Art:
	
	"Sky Backdrop" (sky-background.png)
	by Bart Kelsey (https://opengameart.org/content/sky-backdrop)
	License(s): CC-BY 3.0 (Creative Commons Attribution 3.0 Unported)
		 CC-BY-SA 3.0 (Creative Commons Attribution-ShareAlike 3.0 Unported)
		 GPL 3.0 (GNU General Public License 3.0)
		 GPL 2.0 (GNU General Public License 2.0)

	"Tileable Interior Mine Background" (mines_BG.png)
	by ramtam (https://opengameart.org/content/tileable-interior-mine-background)
	License: CC-BY-SA 3.0 (Creative Commons Attribution-ShareAlike 3.0 Unported)

	The Hell part of "TileSet.png" is a derivative work made from "Jungle tiles" (http://opp.opengameart.org/index.php/Jungle_tiles)
	by Open Game Art.org (Hapiel, Damian, Dawnbringer, Ellian, Squirrelsquid, RileyFiery, Gabriel, Numberplay, Skeddles, a3um,
	Surt, Stava, Scarab, guima1901, Conzeit, gogglecrab, anodomani, yaomon17, Alis, Letmething, Grimsane,
	Diggyspiff, PypeBros, pistachio, nickthem, Crow, Buch, Jetrel, Kazen, Noaqh, Darchangel;
	here's a list of all the contributors and their personal links: http://opp.opengameart.org/index.php/Contributors)
	License: CC-BY 3.0 (Creative Commons Attribution 3.0 Unported)

Musics and Audio:

	"Bleeps and Synths" (title.ogg)
	https://opengameart.org/content/bleeps-and-synths
	by Pant0don (https://soundcloud.com/3xblast)
	License: CC-BY 3.0 (Creative Commons Attribution 3.0 Unported)

	"A Journey Awaits" (level.ogg)
	by Pierre Bondoerffer (@pbondoer)
	License: CC-BY-SA 3.0 (Creative Commons Attribution-ShareAlike 3.0 Unported)

	"Hold the Line" (hell.ogg)
	by Bart Kelsey (https://opengameart.org/content/hold-line-lospec-mix)
	License: CC-BY 3.0 (Creative Commons Attribution 3.0 Unported)

Fonts:

	https://fontlibrary.org/en/font/jellee-typeface
	by Alfredo Marco Pradil
	License: OFL (SIL Open Font License)



The "LICENSE" file refers to all parts of the code made specifically for this game by Xecestel (Celeste Privitera).

Other parts of the code are licensed by respective authors, as listed below:

- The content in "howToPlay", "credits", "title", "utils", "helperPack" directory is work (of derative work based on it) of Jason Lothamer licensed as stated in the "LICENSE_GameStarter" file in the "helperPack" directory.

- The content in "paulloz.colorblindess" is work of Paul Joannon licensed as stated in the "LICENSE_ColorblindnessPlugin" file in the same directory.



# Changelog

### Version 0.3

- Fixed Lilith's compenetration bug.
- Fixed Level 10 Hell TileMap.
- Fixed a bug which caused the game to try to load a non-existing script.

### Version 0.3.2

- Enhanced controller support for GUI.
- Fixed a bug which caused Lilith to leap when not moving.
- Minor bug fixes.

### Version 0.4

- Implemented Options Menu.
- Fixed bug that caused the player to fall outside of map.
- Modified Pause Menu (Deleted Options Button and Restart Level Button)

### Version 0.4.2

- Fixed Pause Menu "Back to Menu" Button bug.

### Version 0.5

- Completed Options Menu.
- ADDED ITALIAN LOCALIZATION.

### Version 0.6

- Fixed a bug in Heaven TileMap's collisions.
- ADDED LEVEL SELECTOR.

### Version 0.7

- ADDED FILE SAVING FEATURE
- Minor bug fixes

### Version 0.7.1

- Save file moved in user directory

### Version 0.7.5

- Added Level 14

### Version 0.8

- Added Level 15

### Version 0.8.2

- Bug fixes

### Version 1.0

- Fixed a bug that made the MovingBlock get stuck in a wall if pushed.
- Added Level19

### Version 1.0.1

- Fixed a bug which prevented to load levels.

### Version 1.1

- Added control remapping scene in the Options Menu.

### Version 1.1.1

- Completed implementation of control remapping.
- Updated tutorials: now they change accordingly to the input device of choice (joypad or keyboard) and accordingly to the control map.
