extends Node2D

#constants#
const THANKS_ID = "THANKS";
const DEATH_COUNT = ["DEATH_COUNT0", "DEATH_COUNT1", "DEATH_COUNT2", "DEATH_COUNT3"];
const MAIN_MENU_ID = "MAIN_MENU";
const EXIT_ID = "EXIT_GAME";

func _ready():
	localization();
	var death_count = GlobalVariables.deaths;
	
	$Deaths.text = tr(DEATH_COUNT[0]) + " " + str(death_count) + " " + tr(DEATH_COUNT[1]);
	
	if (death_count != 1):
		$Deaths.text += tr(DEATH_COUNT[3]);
	else:
		$Deaths.text += tr(DEATH_COUNT[2]);
	
	$Deaths.text += "!";
	
	$VBoxContainer/mainMenuBtn.grab_focus();
#end

func localization() -> void:
	$Thanks.text = tr(THANKS_ID);
	$VBoxContainer/mainMenuBtn.text = tr(MAIN_MENU_ID);
	$VBoxContainer/exitBtn.text = tr(EXIT_ID);
func _on_exitBtn_pressed():
	get_tree().quit()