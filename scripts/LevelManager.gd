extends Node2D

var check_mouse_input = false;

signal is_pressed;

# Called when the node enters the scene tree for the first time.
func _ready():
	$HUD.set_visible(true);
	AudioServer.set_bus_mute(AudioServer.get_bus_index("SoundTrack"), true);
	$HeavenMusic.play();
	pass

#if the player teleports between worlds
#checks if we're going to Hell or to Heaven
#if we're going to Hell, makes visible every node in group "Hell"
#and makes invisible every node in group "Heaven"
#else if we're going to Heaven, makes visible every node in group "Heaven"
#and makes invisible every node in group "Hell"
#"heaven" and "hell" are spelling error correction
#
#Note that we don't need to change anything else
#as we're using different collision layers and mask
#for different worlds, so the only difference stays
#in the visibility of the nodes
func _on_Player_teleport(in_hell : bool):
	for child in self.get_children():
		if (in_hell):
			if (child.is_in_group("Hell") || child.is_in_group("hell")):
				child.visible = true;
			elif (child.is_in_group("Heaven") || child.is_in_group("heaven")):
				child.visible = false;
			$HeavenMusic.stop();
			$HellMusic.play();
		else:
			if (child.is_in_group("Hell") || child.is_in_group("hell")):
				child.visible = false;
			elif (child.is_in_group("Heaven") || child.is_in_group("heaven")):
				child.visible = true;
			$HellMusic.stop();
			$HeavenMusic.play();
	pass
	
func _input(event):
	if (check_mouse_input):
		if (event is InputEventMouseButton && event.is_pressed()):
			emit_signal("is_pressed", event.button_index);