extends Node

#constants#
const PATH = "user://savegame.save"
##########

func save_game() -> void:
	var save_game = File.new();
	save_game.open(PATH, File.WRITE);
	var data = GlobalVariables.save();
	save_game.store_line(to_json(data));
	save_game.close();
	
func load_game() -> int:
	print(OS.get_user_data_dir());
	var save_game = File.new()
	if not save_game.file_exists(PATH):
		return 1#Error! Nothing to load!
	
	save_game.open(PATH, File.READ);
	var current_line = parse_json(save_game.get_line())
	for i in current_line.keys():
		if (i == "filename"):
			continue
		GlobalVariables.set(i, current_line[i]);
	save_game.close();
	return 0;