extends Node

#variables#
var items_menu_visible	= true;
var option_menu_visible	= true;
var active_button 		= null;
var active_rect			= null;

onready var heaven_button_texture	= preload("res://assets/sprites/Props/MovingBlock/MovingBlock.png");
onready var hell_button_texture		= preload("res://assets/sprites/Props/MovingBlock/HellBlock.png");

# Called when the node enters the scene tree for the first time.
func _ready():
	$Level/HUD.set_visible(false);
	$Level.check_mouse_input = true;
	$ItemsMenu/GridContainer/Item.grab_focus();

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

#func _input(event):
#	if (event is InputEventMouseButton && event.is_pressed()):
#		pass
			


func _on_ItemsButton_pressed():
	if (items_menu_visible):
		$ItemsButton.text = ">>";
		$ItemsMenu.rect_position.x = -($ItemsMenu.rect_size.x);
		$ItemsButton.rect_position.x = 0;
		self.items_menu_visible = false;
	else:
		$ItemsButton.text = "<<";
		$ItemsMenu.rect_position.x = 0;
		$ItemsButton.rect_position.x = $ItemsMenu.rect_size.x;
		self.items_menu_visible = true;


func _on_OptionButton_pressed():
	if (option_menu_visible):
		$OptionButton.text = "<<";
		$OptionMenu.rect_position.x = $OptionMenu.rect_position.x + $OptionMenu.rect_size.x;
		$OptionButton.rect_position.x = $OptionMenu.rect_position.x - $OptionButton.rect_size.x;
		self.option_menu_visible = false;
	else:
		$OptionButton.text = ">>";
		$OptionMenu.rect_position.x = $OptionMenu.rect_position.x - $OptionMenu.rect_size.x;
		$OptionButton.rect_position.x = $OptionMenu.rect_position.x - $OptionButton.rect_size.x;
		self.option_menu_visible = true;

func _on_Item_being_pressed(item):
	if (active_button != null):
		active_button.pressed = false;
	active_button = item;
	


func _on_HellHeavenBtn_toggled(button_pressed):
	$Level._on_Player_teleport(button_pressed);


func _on_Level_is_pressed(button_index):
	if (button_index == BUTTON_LEFT):
		if (active_button == null):
			var pos = $Level/HeavenTile.get_global_mouse_position() / (GlobalVariables.UNIT_SIZE*2);
			$Level/HeavenTile.set_cell(pos.x, pos.y, 0)
		elif(active_button.id_name == "Lilith"):
			var item_scene	= load("res://scenes/entities/lilith/Player.tscn");
			var item_node	= item_scene.instance();
			var pos = $Level/HeavenTile.get_global_mouse_position();
			item_node.position = pos;
			$Level.add_child(item_node);
			print(pos);
			print(item_node.position);
	elif (button_index == BUTTON_RIGHT):
		var pos = $Level/HeavenTile.get_global_mouse_position() / (GlobalVariables.UNIT_SIZE*2);
		$Level/HeavenTile.set_cell(pos.x, pos.y, -1)