extends Node

#constants#
const JOY_BUTTONS = {
	0			: "A",
	1			: "B",
	2			: "X",
	3			: "Y",
	10			: "Select",
	11			: "Start",
	12			: "UP",
	13			: "DOWN",
	14			: "LEFT",
	15			: "RIGHT",
	4			: "LB",
	6			: "LT",
	8			: "L3",
	5			: "RB",
	7			: "RT",
	9			: "R3",
	};
	
const KEYBOARD_BUTTONS = {
	16777217	: "Esc",
	16777218	: "Tab",
	16777220	: "Backspace",
	16777221	: "ENTER",
	16777222	: "Enter (KeyPad)",
	16777223	: "Ins",
	16777224	: "DEL",
	16777225	: "PAUSE",
	16777226	: "PRINT",
	16777229	: "Home",
	16777230	: "END",
	16777231	: "LEFT",
	16777232	: "UP",
	16777233	: "RIGHT",
	16777234	: "DOWN",
	16777235	: "PAGEUP",
	16777236	: "PAGEDOWN",
	16777237	: "Shift",
	16777238	: "Ctrl",
	16777240	: "Alt",
	16777241	: "CapsLock",
	16777242	: "NUMLOCK",
	16777243	: "SCROLLLOCK",
	16777244	: "F1",
	16777245	: "F2",
	16777246	: "F3",
	16777247	: "F4",
	16777248	: "F5",
	16777249	: "F6",
	16777250	: "F7",
	16777251	: "F8",
	16777252	: "F9",
	16777253	: "F10",
	16777254	: "F11",
	16777255	: "F12",
	16777256	: "F13",
	16777257	: "F14",
	16777258	: "F15",
	16777259	: "F16",
	16777345	: "* (KeyPad)",
	16777346	: "/ (KeyPad)",
	16777347	: "- (KeyPad)",
	16777348	: ". (KeyPad)",
	16777349	: "+ (KeyPad)",
	16777350	: "0 (KeyPad)",
	16777351	: "1 (KeyPad)",
	16777352	: "2 (KeyPad)",
	16777353	: "3 (KeyPad)",
	16777354	: "4 (KeyPad)",
	16777355	: "5 (KeyPad)",
	16777356	: "6 (KeyPad)",
	16777357	: "7 (KeyPad)",
	16777358	: "8 (KeyPad)",
	16777359	: "9 (KeyPad)",
	16777260	: "Super-L",
	16777261	: "Super-R",
	16777262	: "Menu",
	16777263	: "Hyper-L",
	16777264	: "Hyper-R",
	16777265	: "HELP",
	16777266	: "Direction-L",
	16777267	: "Direction-R",
	16777280	: "Back",
	16777281	: "Forward",
	16777282	: "Stop",
	16777283	: "Refresh",
	16777284	: "VOLUMEDOWN",
	16777285	: "MUTEVOLUME",
	16777286	: "VOLUMEUP",
	16777287	: "Bass Boost",
	16777288	: "BassUp",
	16777289	: "BassDown",
	16777290	: "Treble Up",
	16777291	: "Treble Down",
	16777292	: "Media Play",
	16777293	: "Media Stop",
	16777294	: "PrevSong",
	16777295	: "NextSong",
	16777296	: "MediaRec",
	16777297	: "HomePage",
	16777298	: "FAVORITES",
	16777299	: "SEARCH",
	16777300	: "Standby",
	16777301	: "Open URL",
	16777302	: "Launch Mail",
	16777303	: "Launch Media",
	16777304	: "Launch Shortcut 0",
	16777305	: "Launch Shortcut 1",
	16777306	: "Launch Shortcut 2",
	16777307	: "Launch Shortcut 3",
	16777308	: "Launch Shortcut 4",
	16777309	: "Launch Shortcut 5",
	16777310	: "Launch Shortcut 6",
	16777311	: "Launch Shortcut 7",
	16777312	: "Launch Shortcut 8",
	16777313	: "Launch Shortcut 9",
	16777314	: "Launch Shortcut A",
	16777315	: "Launch Shortcut B",
	16777316	: "Launch Shortcut C",
	16777317	: "Launch Shortcut D",
	16777318	: "Launch Shortcut E",
	16777319	: "Launch Shortcut F",
	33554431	: "UNKNOWNKEY",
	
	32			: "SPACEBAR",
	33			: "!",
	34			: "\"",
	35			: "#",
	36			: "$",
	37			: "%",
	38			: "&",
	39			: "'",
	40			: "(",
	41			: ")",
	42			: "*",
	43			: "+",
	44			: ",",
	45			: "-",
	46			: ".",
	47			: "/",
	48			: "0",
	49			: "1",
	50			: "2",
	51			: "3",
	52			: "4",
	53			: "5",
	54			: "6",
	55			: "7",
	56			: "8",
	57			: "9",
	58			: ":",
	59			: ";",
	60			: "<",
	61			: "=",
	62			: ">",
	63			: "?",
	64			: "@",
	65			: "A",
	66			: "B",
	67			: "C",
	68			: "D",
	69			: "E",
	70			: "F",
	71			: "G",
	72			: "H",
	73			: "I",
	74			: "J",
	75			: "K",
	76			: "L",
	77			: "M",
	78			: "N",
	79			: "O",
	80			: "P",
	81			: "Q",
	82			: "R",
	83			: "S",
	84			: "T",
	85			: "U",
	86			: "V",
	87			: "W",
	88			: "X",
	89			: "Y",
	90			: "Z",
	91			: "[",
	92			: "\\",
	93			: "]",
	94			: "^",
	95			: "_",
	96			: "”",
	123			: "{",
	124			: "|",
	125			: "}",
	126			: "~",
	160			: " ",
	161			: "¡",
	162			: "¢",
	163			: "£",
	166			: "¦",
	167			: "§",
	168			: "¨",
	169			: "©",
	171			: "«",
	172			: "»",
	173			: "‐",
	174			: "®",
	176			: "°",
	177			: "±",
	178			: "²",
	179			: "³",
	180			: "´",
	181			: "µ",
	183			: "·",
	184			: "¬",
	185			: "¹",
	187			: "»",
	188			: "¼",
	189			: "½",
	190			: "¾",
	191			: "¿",
	192			: "à",
	193			: "á",
	194			: "â",
	195			: "ã",
	196			: "ä",
	197			: "å",
	198			: "æ",
	199			: "ç",
	200			: "è",
	201			: "é",
	202			: "ê",
	203			: "ë",
	204			: "ì",
	205			: "í",
	206			: "î",
	207			: "ï",
	208			: "ð",
	209			: "ñ",
	210			: "ò",
	211			: "ó",
	212			: "ô",
	213			: "õ",
	214			: "ö",
	215			: "×",
	216			: "ø",
	217			: "ù",
	218			: "ú",
	219			: "û",
	220			: "ü",
	221			: "ý",
	222			: "þ",
	223			: "ß",
	247			: "÷",
	255			: "ÿ",
	
	1			: "LMB",
	2			: "RMB",
	3			: "MMB",
	4			: "MOUSEWHEELUP",
	5			: "MOUSEWHEELDOWN",
	6			: "MOUSEWHEELLEFT",
	7			: "MOUSEWHEELRIGHT",
	8			: "EXTRAMOUSE1",
	9			: "EXTRAMOUSE2"
	};


const UNIT_SIZE		=	32; #la dimensione di un tile
const MAX_LEVELS	=	21;
const LANGUAGES		=	["en", "it"];
##########

#variables#
var deaths			=	0; #number of player's deaths
var cleared_levels	=	0; #number of levels cleared by player

var bgm_level			=	80;
var sfx_level			=	80;
var mute				=	false;
var language_id			=	0;
var window_mode_id		=	0;
var vsync_enabled		=	true;

enum Actions { LEFT, RIGHT, JUMP, TELEPORT, RESET, PAUSE, RUN };
var keyboard_controls = [ 65, 68, 32, 81, 82, 16777217, 16777237 ];
var joypad_controls = [ 14, 15, 0, 6, 3, 11, 7 ];
###########

#functions#

func _ready():
	TranslationServer.set_locale(self.LANGUAGES[self.language_id]);
	pass

#loads data from file
func load_data() -> void:
	TranslationServer.set_locale(self.LANGUAGES[self.language_id]);
	
	if (self.mute):
		AudioServer.set_bus_mute(AudioServer.get_bus_index("Master"), true);
	else:
		var bgm_db = (bgm_level * 104 / 100) - 80;
		AudioServer.set_bus_volume_db(AudioServer.get_bus_index("BGM"), bgm_db);
		AudioServer.set_bus_volume_db(AudioServer.get_bus_index("SoundTrack"), bgm_db);
		
		var sfx_db = (sfx_level * 104 / 100) - 80;
		AudioServer.set_bus_volume_db(AudioServer.get_bus_index("SFX"), sfx_db);
		
	#changes window mode
	if (self.window_mode_id == 0): #if fullscreen selected
		OS.window_borderless = false;
		OS.window_fullscreen = false;
		
	if (self.window_mode_id == 1): #if borderless selected
		OS.window_fullscreen = false;
		OS.window_borderless = true;

	if (self.window_mode_id == 2): #if window mode selected
		OS.window_borderless = false;
		OS.window_fullscreen = true;
	
	OS.vsync_enabled = self.vsync_enabled; #enables/disables vsync
	
	#Input map data loading
	##keyboard
	ProjectSettings.get_setting("input/left").values()[1][0].scancode		= keyboard_controls[Actions.LEFT];
	ProjectSettings.get_setting("input/right").values()[1][0].scancode		= keyboard_controls[Actions.RIGHT];
	ProjectSettings.get_setting("input/jump").values()[1][0].scancode			= keyboard_controls[Actions.JUMP];
	ProjectSettings.get_setting("input/teleport").values()[1][0].scancode		= keyboard_controls[Actions.TELEPORT];
	ProjectSettings.get_setting("input/reset").values()[1][0].scancode			= keyboard_controls[Actions.RESET];
	ProjectSettings.get_setting("input/pause").values()[1][0].scancode		= keyboard_controls[Actions.PAUSE];
	##controller
	ProjectSettings.get_setting("input/left").values()[1][1].button_index	= joypad_controls[Actions.LEFT];
	ProjectSettings.get_setting("input/right").values()[1][1].button_index	= joypad_controls[Actions.RIGHT];
	ProjectSettings.get_setting("input/jump").values()[1][1].button_index		= joypad_controls[Actions.JUMP];
	ProjectSettings.get_setting("input/teleport").values()[1][1].button_index	= joypad_controls[Actions.TELEPORT];
	ProjectSettings.get_setting("input/reset").values()[1][1].button_index		= joypad_controls[Actions.RESET];
	ProjectSettings.get_setting("input/pause").values()[1][1].button_index	= joypad_controls[Actions.PAUSE];
#end

func add_death() -> void:
	self.deaths += 1;
	
#This function can be useful to implement a saving
#feature.
func clear_level(level) -> void:
	if (cleared_levels == MAX_LEVELS || cleared_levels >= level):
		return;
	self.cleared_levels += 1
	SaveLoad.save_game();
	
func save():
	var save_dict = {
		"filename"				:	get_filename(),
		"deaths"				:	deaths,
		"cleared_levels"		:	cleared_levels,
		"bgm_level"				:	bgm_level,
		"sfx_level"				:	sfx_level,
		"mute"					:	mute,
		"language_id"			:	language_id,
		"window_mode_id"		:	window_mode_id,
		"vsync_enabled"			:	vsync_enabled,
		"keyboard_controls"		:	keyboard_controls,
		"joypad_controls"		:	joypad_controls
		}
	return save_dict;
