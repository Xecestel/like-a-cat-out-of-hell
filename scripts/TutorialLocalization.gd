extends CenterContainer

#constants#
var TUTORIAL_KEYBOARD_IDS	=	[
	tr("PRESS") + " " + tr(GlobalVariables.KEYBOARD_BUTTONS.get(ProjectSettings.get_setting("input/jump").values()[1][0].scancode)) + " " + tr("TUTORIAL_JUMP"),
	tr("HOLD") + " " + tr(GlobalVariables.KEYBOARD_BUTTONS.get(ProjectSettings.get_setting("input/run").values()[1][0].scancode)) + " " + tr("TUTORIAL_RUN") + tr("PRESS") + " " + tr(GlobalVariables.KEYBOARD_BUTTONS.get(ProjectSettings.get_setting("input/jump").values()[1][0].scancode)) + " " + tr("TUTORIAL_LEAP") ,
	tr("PRESS") + " " + tr(GlobalVariables.KEYBOARD_BUTTONS.get(ProjectSettings.get_setting("input/teleport").values()[1][0].scancode)) + " " + tr("TUTORIAL_TELEPORT"),
	tr("TUTORIAL_PUSH") + tr("PRESS") + " " + tr(GlobalVariables.KEYBOARD_BUTTONS.get(ProjectSettings.get_setting("input/reset").values()[1][0].scancode)) + " " + tr("TUTORIAL_RESET"),
	"TUTORIAL_DEMONS",
	"TUTORIAL_ENEMIES",
	tr("TUTORIAL_MANY_TIMES0") + " (" + tr(GlobalVariables.KEYBOARD_BUTTONS.get(ProjectSettings.get_setting("input/teleport").values()[1][0].scancode)) + ")\n" + tr("TUTORIAL_MANY_TIMES1"),
	"TUTORIAL_FLY"
	];
	
var TUTORIAL_JOYPAD_IDS	=	[
	tr("PRESS") + " " + tr(GlobalVariables.JOY_BUTTONS.get(ProjectSettings.get_setting("input/jump").values()[1][1].button_index)) + " " + tr("TUTORIAL_JUMP"),
	tr("HOLD") + " " + tr(GlobalVariables.JOY_BUTTONS.get(ProjectSettings.get_setting("input/run").values()[1][1].button_index)) + " " + tr("TUTORIAL_RUN") + tr("PRESS") + " " + tr(GlobalVariables.JOY_BUTTONS.get(ProjectSettings.get_setting("input/jump").values()[1][1].button_index)) + " " + tr("TUTORIAL_LEAP") ,
	tr("PRESS") + " " + tr(GlobalVariables.JOY_BUTTONS.get(ProjectSettings.get_setting("input/teleport").values()[1][1].button_index)) + " " + tr("TUTORIAL_TELEPORT"),
	tr("TUTORIAL_PUSH") + tr("PRESS") + " " + tr(GlobalVariables.JOY_BUTTONS.get(ProjectSettings.get_setting("input/reset").values()[1][1].button_index)) + " " + tr("TUTORIAL_RESET"),
	tr("TUTORIAL_DEMONS"),
	tr("TUTORIAL_ENEMIES"),
	tr("TUTORIAL_MANY_TIMES0") + " (" + tr(GlobalVariables.JOY_BUTTONS.get(ProjectSettings.get_setting("input/teleport").values()[1][1].button_index)) + ")\n" + tr("TUTORIAL_MANY_TIMES1"),
	tr("TUTORIAL_FLY")
	];
###########

#variables#
export(int) var current_tutorial = -1;
###########

# Called when the node enters the scene tree for the first time.
func _ready():
	Input.connect("joy_connection_changed", self, "_on_joy_connection_changed");
	self.localization();
	
	
func localization() -> void:
	if (current_tutorial >= 0):
		if (Input.get_joy_name(0)):
			$Label.text = TUTORIAL_JOYPAD_IDS[current_tutorial];
		else:
			$Label.text = TUTORIAL_KEYBOARD_IDS[current_tutorial];
		
func _on_joy_connection_changed(device_id, connected) -> void:
	self.localization();


