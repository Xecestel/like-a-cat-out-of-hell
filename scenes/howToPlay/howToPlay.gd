extends TextureRect

#constants#
const TITLE_ID	= "HOW_TO_PLAY";
const BACK_ID 	= "BACK";
const PLAY_ID	= "PLAY";
##########

func _ready():
	localization();
	GlobalVariables.deaths = 0;
	$HBoxContainer/backBtn.grab_focus();
	pass
	
func localization() -> void:
	$howToPlayLbl.text			= tr(TITLE_ID);
	$HBoxContainer/backBtn.text	= tr(BACK_ID);
	$HBoxContainer/playBtn.text	= tr(PLAY_ID);

func _input(event):
	if (event.is_action_pressed("ui_cancel")):
		transitionMgr.transitionTo("res://scenes/title/title.tscn", 2.0, false)
