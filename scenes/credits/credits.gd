extends TextureRect

#constants#
const TITLE_ID	= "CREDITS";
const BACK_ID	= "BACK";
##########

#variables#
onready var file_display = $fileContentDisplay;
###########


func _ready():
	localization();
	$backBtn.grab_focus();
	$fileContentDisplay.cursor_set_line(2);
	pass
	
func _process(delta):
	if (Input.is_action_pressed("ui_scroll_down")):
		file_display.cursor_set_line(file_display.cursor_get_line() + 1);
	if (Input.is_action_pressed("ui_scroll_up")):
		file_display.cursor_set_line(file_display.cursor_get_line() - 1);
		
func localization() -> void:
	$CreditsLbl.text	= tr(TITLE_ID);
	$backBtn.text		= tr(BACK_ID);

func _input(event):
	if (event.is_action_pressed("ui_cancel")):
		transitionMgr.transitionTo("res://scenes/title/title.tscn", 2.0, false)
#	if (event.is_action_pressed("ui_scroll_down")):
#		file_display.cursor_set_line(file_display.cursor_get_line() + 1);
#	if (event.is_action_pressed("ui_scroll_up")):
#		file_display.cursor_set_line(file_display.cursor_get_line() - 1);
		

func _on_fileContentDisplay_gui_input(event):
	_input(event);
	pass
