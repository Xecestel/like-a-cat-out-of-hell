extends CanvasLayer

#constants#
const LEVEL_NAME	= "LEVEL";
const DEATH			= "DEATHS";
const HELL			= "HELL";
const HEAVEN		= "HEAVEN";

#variables#
export 	var current_level	=	1;
onready var heaven_font 	=	preload("res://scenes/UI/HeavenFont.tres");
onready var hell_font		=	preload("res://scenes/UI/HellFont.tres");

# Called when the node enters the scene tree for the first time.
func _ready():
	$LevelName.text = tr(LEVEL_NAME) + " " + str(current_level) + "\n" + tr(HEAVEN);
	self.update_death_count();

#switch text label in HUD
func _on_Player_teleport(in_hell : bool) -> void:
	$LevelName.text = tr(LEVEL_NAME) + " " + str(current_level) + "\n";
	
	if (in_hell):
		$LevelName.text += tr(HELL);
		$LevelName.set("custom_fonts/font", hell_font);
		$LevelName.set("custom_colors/font_color", "#000000");
		$DeathNumber.set("custom_fonts/font", hell_font);
		$DeathNumber.set("custom_colors/font_color", "#000000");
	else:
		$LevelName.text += tr(HEAVEN);
		$LevelName.set("custom_fonts/font", heaven_font);
		$LevelName.set("custom_colors/font_color", "#ffffff");
		$DeathNumber.set("custom_fonts/font", heaven_font);
		$DeathNumber.set("custom_colors/font_color", "#ffffff");
		
func _on_Player_death() -> void:
	self.update_death_count();
	
func update_death_count() -> void:
	$DeathNumber.text = tr(DEATH) + ": " + str(GlobalVariables.deaths);
	
func set_visible(visible : bool) -> void:
	$LevelName.visible = visible;
	$DeathNumber.visible = visible;
