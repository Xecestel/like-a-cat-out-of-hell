extends TextureRect

#constants#
const NEWGAME_ID 	= "NEW_GAME";
const LEVEL_SEL		= "LEVEL_SELEC";
const OPTIONS_ID 	= "OPTIONS";
const CREDITS_ID 	= "CREDITS";
const EXIT_ID		= "EXIT_GAME";
###########


func _ready():	
	#if there's something to load
	if (SaveLoad.load_game() == 0):
		GlobalVariables.load_data(); #loads data from GlobalVariables
	
	self.localization();
	$VBoxContainer/newGameBtn.grab_focus();
	AudioServer.set_bus_mute(AudioServer.get_bus_index("SoundTrack"), false);

func localization() -> void:
	$VBoxContainer/newGameBtn.text 	= tr(NEWGAME_ID);
	$VBoxContainer/lvlSelecBtn.text = tr(LEVEL_SEL);
	$VBoxContainer/optionsBtn.text 	= tr(OPTIONS_ID);
	$VBoxContainer/creditsBtn.text 	= tr(CREDITS_ID);
	$VBoxContainer/exitBtn.text		= tr(EXIT_ID);

func _on_exitBtn_pressed():
	get_tree().quit()

func _on_LinkButton_pressed():
	OS.shell_open("https://gitlab.com/Xecestel/like-a-cat-out-of-hell");
