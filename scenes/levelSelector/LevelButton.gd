extends Panel

#constants#
const LEVEL_ID	= "LEVEL";
const LEVELS_DIR = "res://scenes/levels/"
const THUMB_DIR = "res://assets/images/LevelThumbnails/"

#signals#
signal grabbing_focus;

#variables#
export(int) var level = 0;

onready var pos = self.rect_global_position;
onready var size = Vector2(110, 130);

# Called when the node enters the scene tree for the first time.
func _ready():
	var texture = THUMB_DIR + self.name + ".png";
	self.localization();
	$LevelButton.texture_normal = load(texture);
	$LevelButton.texture_disabled = load(texture);
	$LevelName.text += " " + str(self.level);
	if (GlobalVariables.cleared_levels + 1 < self.level):
		self.modulate = Color(1, 1, 1, 0.5);
		$LevelButton.disabled = true;
	if (level > GlobalVariables.MAX_LEVELS):
		$LevelName.visible = false;
		
func localization() -> void:
	$LevelName.text = tr(LEVEL_ID);


func _on_LevelButton_pressed():
	var scene_to_load = LEVELS_DIR + self.name + ".tscn";
	$Select.play();
	transitionMgr.transitionTo(scene_to_load, 2.0, true);
	
func grab_focus():
	$TextureRect.visible = true;

func lose_focus():
	$TextureRect.visible = false;
	
func get_level() -> int:
	return self.level;
	
func get_pos() -> Vector2:
	return self.pos;
	
func get_size() -> Vector2:
	return self.size;
	
func press_button() -> void:
	if ($LevelButton.disabled):
		$Buzzer.play();
	else:
		self._on_LevelButton_pressed();
		
func _input(event):
	if (event is InputEventMouseButton && event.is_pressed() &&
		event.position.x >= self.get_pos().x &&
		event.position.y >= self.get_pos().y &&
		event.position.x <= self.get_pos().x + self.get_size().x &&
		event.position.y <= self.get_pos().y + self.get_size().y):
			if ($TextureRect.visible):
				self.press_button();
			else:
				emit_signal("grabbing_focus", self);
