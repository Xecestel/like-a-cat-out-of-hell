extends Control

#constants#
const TITLE_ID = "LEVEL_SELEC";
const BACK_BTN = "BACK";

#variables#
var active_button = null;

# Called when the node enters the scene tree for the first time.
func _ready():
	self.localization();
	active_button = $MarginContainer/ColorRect/GridContainer/Level0;
	active_button.grab_focus();

func localization() -> void:
	$Title.text = tr(TITLE_ID);
	$backBtn.text = tr(BACK_BTN);

func _input(event):	
	if (event.is_action_pressed("ui_right")):
		if (active_button.get_level() <= $MarginContainer/ColorRect/GridContainer.get_child_count() - 1):
			var next_button = $MarginContainer/ColorRect/GridContainer.get_children()[active_button.get_index() + 1];
			active_button.lose_focus();
			active_button = next_button;
			active_button.grab_focus();
			
	if (event.is_action_pressed("ui_left")):
		if (active_button.get_level() > 1):
			var next_button = $MarginContainer/ColorRect/GridContainer.get_children()[active_button.get_index() - 1];
			active_button.lose_focus();
			active_button = next_button;
			active_button.grab_focus();
	
	if (event.is_action_pressed("ui_down")):
		if (active_button.get_level() <= $MarginContainer/ColorRect/GridContainer.get_child_count() - 7):
			var next_button = $MarginContainer/ColorRect/GridContainer.get_children()[active_button.get_index() + 7];
			active_button.lose_focus();
			active_button = next_button;
			active_button.grab_focus();
			
	if (event.is_action_pressed("ui_up")):
		if (active_button.get_level() > 1):
			var next_button = $MarginContainer/ColorRect/GridContainer.get_children()[active_button.get_index() - 7];
			active_button.lose_focus();
			active_button = next_button;
			active_button.grab_focus();
			
			
	if (event.is_action_pressed("ui_accept")):
		active_button.press_button();
	pass


func _on_Level_grabbing_focus(next_button):
	active_button.lose_focus();
	active_button = next_button;
	active_button.grab_focus();
	pass
