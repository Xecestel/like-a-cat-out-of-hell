extends Control

#constants#
const RESUME_ID		= "RESUME";
const OPTIONS_ID	= "OPTIONS";
const RESTART_ID	= "RESTART";
const QUIT_ID		= "QUIT";

const CONFIRM_TITLE		= "YOU_SURE";
const YES_ID			= "YES";
const NO_ID				= "NO";
const CONFIRM_QUIT		= "CONFIRM_QUIT";
const CONFIRM_RESTART	= "CONFIRM_RESTART";

#variables#
var world;

#boolean variables for confirmation options
var quitting_game				=	false;
var restarting_level			=	false;
###########

func _ready():
	self.localization();
	$ColorRect/Commands/Resume.grab_focus();
	
	$ConfirmationDialog.get_cancel().text = tr(NO_ID);
	$ConfirmationDialog.get_ok().text = tr(YES_ID);
	pass

func _process(delta):
	# Called every frame. Delta is time since last frame.
		
	#if cancel button is pressed on confirmation dialog
	#resets boolean switches
	if ($ConfirmationDialog.visible):
		if($ConfirmationDialog.get_cancel().pressed):
			if(self.quitting_game):
				self.quitting_game					=	false;
				
			if (self.restarting_level):
				self.restarting_level				=	false;
				
			pass
		pass
		
	pass

#called when an input event is triggered
func _input(event):
	if (event.is_action_pressed("pause") && $ConfirmationDialog.visible == false): #if pause is pressed
		get_tree().paused = not get_tree().paused; #pause/unpause the game
		self.visible = not self.visible; #show/hide the menu
		$ColorRect/Commands/Resume.grab_focus();
	pass

func _on_RestartLevel_pressed():
	$ConfirmationDialog.dialog_text = tr(CONFIRM_RESTART);
	$ConfirmationDialog.popup_centered();
	self.restarting_level = true;
	pass
	
func _on_QuitGame_pressed():
	$ConfirmationDialog.dialog_text = tr(CONFIRM_QUIT);
	$ConfirmationDialog.popup_centered();
	self.quitting_game = true;
	pass;

func _on_Resume_pressed():
	close_menu();
	pass
	
#closes the menu();
func close_menu():
	self.visible = false;
	get_tree().paused = false;
	pass
	

func _on_Options_pressed():
	pass # replace with function body


func _on_ConfirmationDialog_confirmed():
	if (self.quitting_game):
		get_tree().change_scene("res://scenes/title/title.tscn");
		close_menu();
		self.quitting_game = false;
		
	if (self.restarting_level):
		GlobalVariables.add_death();
		self.get_tree().reload_current_scene();
		close_menu();
		self.restarting_level = false;
		
func localization() -> void:
	$ColorRect/Commands/Resume.text = tr(RESUME_ID);
	$ColorRect/Commands/Options.text = tr(OPTIONS_ID);
	$ColorRect/Commands/RestartLevel.text = tr(RESTART_ID);
	$ColorRect/Commands/QuitGame.text = tr(QUIT_ID);
	$ConfirmationDialog.window_title = tr(CONFIRM_TITLE);
