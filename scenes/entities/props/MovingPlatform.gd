extends Node2D

#constants#
const IDLE_DURATION		=	1.0;

#variables#
onready var platform	=	$Platform;
onready var tween		=	$MoveTween;

export(Vector2) var move_to		=	Vector2.RIGHT * 192;
export(float) var speed			=	3.0;
export(bool) var moving			=	true;

var follow						=	Vector2.ZERO;
var tween_inited				=	false;
##########

# Called when the node enters the scene tree for the first time.
func _ready():
	self._init_tween(); #initializes tween animation
	tween.set_active(self.moving); #if the object should move, the tween is set active
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	platform.position = platform.position.linear_interpolate(follow, 0.075); #makes the movement smoother
	
func _init_tween():
	var duration = move_to.length() / float(speed * GlobalVariables.UNIT_SIZE);
	tween.interpolate_property(self, "follow", Vector2.ZERO, move_to, duration, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT, IDLE_DURATION);
	tween.interpolate_property(self, "follow", move_to, Vector2.ZERO, duration, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT, duration+IDLE_DURATION*2);
	tween.start();
	tween_inited = true;
	
func _on_Button_pressed() -> void:
	tween.set_active(!tween.is_active());
	
func _on_Button_unpressed() -> void:
	tween.set_active(!tween.is_active());