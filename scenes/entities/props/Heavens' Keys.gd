extends Area2D

#variables#
export(int) var next_level = 1;


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

#when a body enters the Area 2D
func _on_Heavens_Keys_body_entered(body):
	#if the body is the player
	if ('Player' in body.name):
		$LevelClearedFanfare.play(); #plays fanfare sound effect
		var curr_level = next_level;
		GlobalVariables.clear_level(curr_level);
		if (next_level == 0):
			transitionMgr.transitionTo("res://scenes/levels/Level20.tscn", 2.0, false);
		else:
			transitionMgr.transitionTo("res://scenes/levels/Level" + str(next_level) + ".tscn", 2.0, false);
	pass
