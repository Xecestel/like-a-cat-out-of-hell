extends Area2D

#constants#
const HEAVEN_COLLISIONS		=	1;
const HELL_COLLISIONS		=	2;
##########

#variables#
var unpressed_texture	=	preload("res://assets/sprites/Props/Button/Button0.png");
var pressed_texture		=	preload("res://assets/sprites/Props/Button/Button1.png");
var pressed				=	false;
###########

#signals#
signal pressed;
signal unpressed;
########

func _ready():
	self.prepare_world();

func _on_body_entered(body):
	if (pressed == false):
		$SFX.play();
		$Sprite.set_texture(pressed_texture);
		emit_signal("pressed");
		pressed = true;


func _on_body_exited(body):
	if (pressed == true):
		$SFX.play();
		$Sprite.set_texture(unpressed_texture);
		emit_signal("unpressed");
		pressed = false;
		
func prepare_world() -> void:
	if (self.is_in_group("Heaven")):
		$Sprite.flip_v = false;
		self.collision_layer = HEAVEN_COLLISIONS;
		self.collision_mask = HEAVEN_COLLISIONS;
	elif (self.is_in_group("Hell")):
		$Sprite.flip_v = true;
		self.collision_layer = HELL_COLLISIONS;
		self.collision_mask = HELL_COLLISIONS;
