extends KinematicBody2D

#In Godot v.3.1 RigidBodies started having serious issues when colliding with walls
#and floors.
#To implement the MovingBlock I then decided to make it a KinematicBody and
#program its movement myself.
#
#To avoid problems of any kind I decided to not make use of the
#physics engine and just make it move horizontally one tile at a time.
#

#constants#
const GRAVITY = 800;
const HEAVEN_COLLISIONS		=	1;
const HELL_COLLISIONS		=	2;
###########

#variables#
onready var heaven_texture = preload("res://assets/sprites/Props/MovingBlock/MovingBlock.png");
onready var hell_texture = preload("res://assets/sprites/Props/MovingBlock/HellBlock.png");
var motion = Vector2();
var fall_played = false; #This is to avoid infinite repetition of the Fall AudioStream
var hell;
###########

# Called when the node enters the scene tree for the first time.
func _ready():
	self.prepare_world();
	$FloorRayCast.collision_mask = self.collision_mask;
	$FloorRayCast2.collision_mask = self.collision_mask;
	$WallRayCast.collision_mask = self.collision_mask;
	pass

func _process(delta):
	if ($FloorRayCast.is_colliding() == false):
		if ($FloorRayCast2.is_colliding() == false):
			self.motion.y += (GRAVITY * $FloorRayCast.cast_to.y / abs($FloorRayCast.cast_to.y));
		else:
			var collider = $FloorRayCast2.get_collider();
			if ("Player" in collider.name):
				collider.process_damage();
			self.motion.y = 0;
			fall_played = false;
	else:
		var collider = $FloorRayCast.get_collider();
		if ("Player" in collider.name):
			collider.process_damage();
	
	if (motion.y > 20000 && fall_played == false):
		$Fall.play();
		fall_played = true;
	
	self.move_and_slide(motion*delta);
	pass

#This function is called by the colliding object (the player)
#when the two bodies collide
#By using the WallRayCast we can avoid the Block to be pushed inside a wall
func push(direction) -> void:
	$WallRayCast.cast_to.x *= direction;
	if ($WallRayCast.is_colliding() == false):
		$Push.play();
		self.position.x += GlobalVariables.UNIT_SIZE * direction * 2;
		
#used to avoid memory leaks of any sort
#free the memory when the object falls outside of the map
func _on_despawn() -> void:
	self.queue_free();
	
func prepare_world() -> void:
	if (self.is_in_group("Heaven")):
		$Sprite.texture = heaven_texture;
		self.collision_layer = HEAVEN_COLLISIONS;
		self.collision_mask = HEAVEN_COLLISIONS;
	elif (self.is_in_group("Hell")):
		$Sprite.texture = hell_texture;
		self.collision_layer = HELL_COLLISIONS;
		self.collision_mask = HELL_COLLISIONS;
		$FloorRayCast.cast_to.y *= -1;
		$FloorRayCast2.cast_to.y *= -1;
		