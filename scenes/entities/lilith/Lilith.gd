extends KinematicBody2D

#constants#
const UP					=	Vector2(0, -1); #normal versor to the ground in heaven
const DOWN					=	Vector2(0, 1); #normal versor to the ground in Hell
const GRAVITY				=	20
const WALKING_MAX_SPEED		=	150;
const RUNNING_MAX_SPEED		=	600;
const JUMP_HEIGHT			=	-700;
const WALKING_ACCELERATION	=	50;
const RUNNING_ACCELERATION	=	150;
const HEAVEN_COLLISIONS		=	1;
const HELL_COLLISIONS		=	2;
const RAYCAST_CAST_TO		=	25;
###########

#signals#
signal teleport;
signal death;
#########

#variables#
var spawn_position = Vector2(); #spawn position of the player after he dies
var motion = Vector2();
var friction = false;
var running = false;
var hell = false;
var is_jumping = false;
var stuck = false;
##########


#functions#
func _ready():
	spawn_position = self.global_position #initialize spawn position
	$InteractionRayCast.collision_mask = self.collision_mask;
	pass

#process function, called every frame
#delta is time between frames
func _physics_process(delta):
	
	if (stuck):
		self.position.x -= GlobalVariables.UNIT_SIZE;
	else:
		#input processing
		self.get_input();
		
		#if the player is in hell
		#the vertical axes are reversed
		#so we need to create two different
		#process functions
		if (self.hell):
			self.hell_process(delta);
		else:
			self.heaven_process(delta);
		
		#RayCast collisions checking
		self.check_collisions(delta);
	
#end

#process physics in heaven
#basically is the normal physics
func heaven_process(delta):
	#simulates gravity
	motion.y += GRAVITY;
	
	#jump processing
	if (self.is_on_floor()):
		if (Input.is_action_just_pressed("jump")):
			self.is_jumping = true;
			$JumpSound.play();
			if (self.running):
				motion.y = JUMP_HEIGHT/1.5; #leap height
				motion.x += motion.x*2; #leap lenght
			else:
				motion.y = JUMP_HEIGHT;
		if (friction == true):
			motion.x = lerp(motion.x, 0, 0.2);
	else:
		if (motion.y < 0): #if player is going up
			$AnimatedSprite.play("jump");
		else: #if player is going down
			$AnimatedSprite.play("falling");
	
	if (self.is_jumping && self.motion.y >= 0):
		self.is_jumping = false;
		
	var snap = Vector2.DOWN * 32 if !is_jumping else Vector2.ZERO;
		
	if (friction == true):
		motion.x = lerp(motion.x, 0, 0.05); #simulates friction
	motion = self.move_and_slide_with_snap(motion, snap, UP); #execute movement
	pass
#end

#process physics in hell
#basically it's the same physics but
#the vertical axes are reversed
func hell_process(delta):
	#simulates gravity
	motion.y -= GRAVITY;
	
	#jump processing
	if (self.is_on_floor()):
		if (Input.is_action_just_pressed("jump")):
			self.is_jumping = true;
			$JumpSound.play();
			if (self.running):
				motion.y = -JUMP_HEIGHT/1.5; #leap height
				motion.x += motion.x*2; #leap lenght
			else:
				motion.y = -JUMP_HEIGHT;
		if (friction == true):
			motion.x = lerp(motion.x, 0, 0.2);
	else:
		if (motion.y > 0): #if player is going up
			$AnimatedSprite.play("jump");
		else: #if player is going down
			$AnimatedSprite.play("falling");

	if (self.is_jumping && self.motion.y <= 0):
		self.is_jumping = false;
		
	var snap = Vector2.UP * 32 if !is_jumping else Vector2.ZERO;

	if (friction == true):
		motion.x = lerp(motion.x, 0, 0.05); #simulates friction
	motion = self.move_and_slide_with_snap(motion, snap, DOWN); #execute movement
#end

#process damage received
#damage is the damage given from the object
#animation is the death animation for the said object
func process_damage():
	#reset player's position
	$DamageSound.play();
	self.hell = false;
	GlobalVariables.add_death();
	emit_signal("death");
	$AnimatedSprite.flip_v = false;
	self.process_teleport(); #switches collisions and resets the map
	global_position	=	spawn_position;
#end

#process the teleport mechanic
#emits a signal to warn other scene objects
#changes player's collision layer and mask
func process_teleport() -> void:
	emit_signal("teleport", self.hell);
	self.switch_collisions();
	motion.y = 0;
#end
	
#changes collision layer and mask
func switch_collisions() -> void:
	if (self.hell):
		self.collision_layer = HELL_COLLISIONS;
		self.collision_mask = HELL_COLLISIONS;
	else:
		self.collision_layer = HEAVEN_COLLISIONS;
		self.collision_mask = HEAVEN_COLLISIONS;
	$InteractionRayCast.collision_mask = self.collision_mask;
	$Area2D.collision_layer = self.collision_layer;
	$Area2D.collision_mask = self.collision_mask;
#end

#checks collision in RayCast for pushable objects
func check_collisions(delta) -> void:
	if ($InteractionRayCast.is_colliding()):
		var collider = $InteractionRayCast.get_collider();
		if ("Pushable" in collider.get_groups()):
			collider.push($InteractionRayCast.cast_to.x / abs($InteractionRayCast.cast_to.x));
#end

###BASIC INPUT PROCESSING###

func get_input() -> void:
	#if player presses "run" button
	#character is running
	if (Input.is_action_pressed("run") && motion != Vector2.ZERO):
		self.running = true;
	else:
		self.running = false;

	#horizontal movement processing
	if (Input.is_action_pressed("right")):
		#if is not running
		if (self.running == false):
			motion.x = min(motion.x + WALKING_ACCELERATION, WALKING_MAX_SPEED); #speed limit
			$AnimatedSprite.flip_h = false;
			$InteractionRayCast.cast_to = Vector2( RAYCAST_CAST_TO , 0 );
			if (self.is_on_floor()):
				$AnimatedSprite.play("walking");
		#if is running
		else:
			motion.x = min(motion.x + RUNNING_ACCELERATION, RUNNING_MAX_SPEED);
			$AnimatedSprite.flip_h = false;
			$InteractionRayCast.cast_to = Vector2( 20 , 0 );
			if (self.is_on_floor()):
				$AnimatedSprite.play("walking");
	elif (Input.is_action_pressed("left")):
		if (self.running == false):
			motion.x = max(motion.x-WALKING_ACCELERATION, -WALKING_MAX_SPEED);
			$AnimatedSprite.flip_h = true;
			$InteractionRayCast.cast_to = Vector2( -RAYCAST_CAST_TO , 0 );
			if (self.is_on_floor()):
				$AnimatedSprite.play("walking");
		else:
			motion.x = max(motion.x - RUNNING_ACCELERATION, -RUNNING_MAX_SPEED);
			$AnimatedSprite.flip_h = true;
			$InteractionRayCast.cast_to = Vector2( 20 , 0 );
			if (self.is_on_floor()):
				$AnimatedSprite.play("walking");
	#if not moving
	else:
		$AnimatedSprite.play("idle");
		friction = true;
		motion.x = lerp(motion.x, 0, 0.2);
	
	if (Input.is_action_just_pressed("teleport")):
		$AnimatedSprite.flip_v = !$AnimatedSprite.flip_v;
		self.hell = !self.hell;
		self.process_teleport();
		
	#reset button
	#reloads current level
	if (Input.is_action_just_pressed("reset")):
		GlobalVariables.add_death();
		get_tree().reload_current_scene();

	######



func _on_Area2D_body_entered(body):
	if ("Tile" in body.name):
		stuck = true;
	pass


func _on_Area2D_body_exited(body):
	if ("Tile" in body.name):
		stuck = false;
	pass
