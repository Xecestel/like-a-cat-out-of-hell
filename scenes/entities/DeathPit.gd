extends Area2D

signal damage;
signal despawn;

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func _on_body_entered(body):
	if ("Player" in body.name):
		emit_signal("damage");
	pass

#When any object besides the player exit the Area2D
#Tells them to despawn
func _on_body_exited(body):
	if (!("Player" in body.name)):
		emit_signal("despawn");
	pass
