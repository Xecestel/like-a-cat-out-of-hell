extends KinematicBody2D

#constants#
const UP = Vector2(0, 1);
##########

#variables#
export var speed = 30;
export var gravity = -20;
var motion = Vector2();
var direction = Vector2(1, 0);
export var move = true;
##########

func _process(delta):
	# Called every frame. Delta is time since last frame.
	motion.y += gravity;
	
	if (move):
		if (self.is_on_floor()):
			motion.x = speed * direction.x;
		
		#movement and collision checking
		if (self.is_on_wall() || $MovementRayCast.is_colliding() == false):
			if ($WallCooldown.time_left == 0):			
				direction.x *= -1;
				$Sprite.flip_h = !($Sprite.flip_h && true);
				$MovementRayCast.position.x *= -1;
				$WallCooldown.start();
		
	
	self.move_and_slide(motion, UP);	


func _on_Area2D_body_entered(body):
	if ("Player" in body.name):
		body.process_damage();

#used to avoid memory leaks of any sort
#free the memory when the object falls outside of the map
func _on_despawn() -> void:
	self.queue_free();
