extends Control

#####################################################################################
"""SCRIPT INSTRUCTIONS"""															#
#####################################################################################
#This script is made to implement a control remapping feature.						#
#It uses specific id codes for every keyboard key and joypad button					#
#which can be found in @GlobalScope.												#
#The script uses two dictionaries to pair every id code with the name				#
#of the button or key. This is useful to give the player a feedback of				#
#what key or button they're choosing.												#
#The dictionaries are placed in GlobalVariables to easy access from other			#
#scripts.																			#
#																					#
#If the player presses one of the button on the scene a window dialogue				#
#will appear, asking them to press a button. The player can choose basically any	#
#button if the engine can recognize it.												#
#Once the button is pressed, the script will automatically update the				#
#ProgectSettings and the button on the scene itself.								#
#Note that the the player can only remap gameplay actions. No ui action is			#
#supported for remapping. This is due to avoid that changes in the input mapping	#
#can get the player stuck in the remap scene itself.								#
#																					#
#Once the player presses the Back button to go back to the Options Menu				#
#the script updates the keys and buttons saved in the GlobalVariables Singleton.	#
#This is important as the GlobalVariables script has the power to save				#
#this changes in the save file. 													#
#The Default button changes all the input map back to the default settings.			#
#####################################################################################

#constants#
######################
"""LOCALIZATION IDS"""
######################

const BACK_ID = "BACK";
const MOUSEKEY_ID = "MOUSEKEYBOARD";
const CONTROLLER_ID = "CONTROLLER";
const YES_ID		= "YES";
const NO_ID		= "NO";
const CONFIRM_DIALOGUE_TEXT = "ACCEPT_CHANGES";

######################


#variables#
var active_key				:	int;
var active_action			=	String();
var active_button;
var active_property_index	:	int;

var keyboard_controls		=	[ 65, 68, 32, 81, 82, 16777217, 16777237 ];
var joypad_controls			=	[ 14, 15, 0, 6, 3, 11, 7 ];
###########

# Called when the node enters the scene tree for the first time.
func _ready():
	self.localize();
	self.draw_buttons();
	
	active_button = $KeyRemap/Tabs/MouseKeyboard/LeftButton;
	active_button.grab_focus();
	
func localize() -> void:
	$KeyRemap/Tabs/Controller/BackButton.text = tr(BACK_ID);
	$KeyRemap/Tabs/MouseKeyboard/BackButton.text = tr(BACK_ID);
	$KeyRemap/Tabs.set_tab_title(0, tr(MOUSEKEY_ID));
	$KeyRemap/Tabs.set_tab_title(1, tr(CONTROLLER_ID));
	
	$ConfirmationDialogue.dialog_text = tr(CONFIRM_DIALOGUE_TEXT);
	$ConfirmationDialogue.get_ok().text = tr(YES_ID);
	$ConfirmationDialogue.get_cancel().text = tr(NO_ID);
	

func draw_buttons() -> void:
	$KeyRemap/Tabs/Controller/LeftButton.text = tr(GlobalVariables.JOY_BUTTONS.get(ProjectSettings.get_setting("input/left").values()[1][1].button_index));
	$KeyRemap/Tabs/Controller/RightButton.text = tr(GlobalVariables.JOY_BUTTONS.get(ProjectSettings.get_setting("input/right").values()[1][1].button_index));
	$KeyRemap/Tabs/Controller/JumpButton.text = tr(GlobalVariables.JOY_BUTTONS.get(ProjectSettings.get_setting("input/jump").values()[1][1].button_index));
	$KeyRemap/Tabs/Controller/RunButton.text = tr(GlobalVariables.JOY_BUTTONS.get(ProjectSettings.get_setting("input/run").values()[1][1].button_index));
	$KeyRemap/Tabs/Controller/TeleportButton.text = tr(GlobalVariables.JOY_BUTTONS.get(ProjectSettings.get_setting("input/teleport").values()[1][1].button_index));
	$KeyRemap/Tabs/Controller/ResetButton.text = tr(GlobalVariables.JOY_BUTTONS.get(ProjectSettings.get_setting("input/reset").values()[1][1].button_index));
	$KeyRemap/Tabs/Controller/PauseButton.text = tr(GlobalVariables.JOY_BUTTONS.get(ProjectSettings.get_setting("input/pause").values()[1][1].button_index));
	
	
	$KeyRemap/Tabs/MouseKeyboard/LeftButton.text = tr(GlobalVariables.KEYBOARD_BUTTONS.get(ProjectSettings.get_setting("input/left").values()[1][0].scancode));
	$KeyRemap/Tabs/MouseKeyboard/RightButton.text = tr(GlobalVariables.KEYBOARD_BUTTONS.get(ProjectSettings.get_setting("input/right").values()[1][0].scancode));
	$KeyRemap/Tabs/MouseKeyboard/JumpButton.text = tr(GlobalVariables.KEYBOARD_BUTTONS.get(ProjectSettings.get_setting("input/jump").values()[1][0].scancode));
	$KeyRemap/Tabs/MouseKeyboard/RunButton.text = tr(GlobalVariables.KEYBOARD_BUTTONS.get(ProjectSettings.get_setting("input/run").values()[1][0].scancode));
	$KeyRemap/Tabs/MouseKeyboard/TeleportButton.text = tr(GlobalVariables.KEYBOARD_BUTTONS.get(ProjectSettings.get_setting("input/teleport").values()[1][0].scancode));
	$KeyRemap/Tabs/MouseKeyboard/ResetButton.text = tr(GlobalVariables.KEYBOARD_BUTTONS.get(ProjectSettings.get_setting("input/reset").values()[1][0].scancode));
	$KeyRemap/Tabs/MouseKeyboard/PauseButton.text = tr(GlobalVariables.KEYBOARD_BUTTONS.get(ProjectSettings.get_setting("input/pause").values()[1][0].scancode));
#end

#this method can be called by calling scenes to make the KeyRemapping scene to grab focus
func grab_focus() -> void:
	$KeyRemap/Tabs/MouseKeyboard/LeftButton.grab_focus();

#This method automatically refreshes the text in the ConfirmationDialogue followin the keys and buttons pressed by the player
func key_chooser_refresh() -> void:
	if ($KeyRemap/Tabs.current_tab == 0):
		$KeyChooser/Dialog.text = "Choose a key. " + tr(GlobalVariables.KEYBOARD_BUTTONS.get(active_key));
	elif ($KeyRemap/Tabs.current_tab == 1):
		$KeyChooser/Dialog.text = "Choose a button: " + tr(GlobalVariables.JOY_BUTTONS.get(active_key));

#This method refreshes the pressed button text with the new chosen key or button
func button_refresh() -> void:
	if ($KeyRemap/Tabs.current_tab == 0):
		active_button.text = tr(GlobalVariables.KEYBOARD_BUTTONS.get(ProjectSettings.get_setting(active_action).values()[1][0].scancode));
	elif ($KeyRemap/Tabs.current_tab == 1):
		active_button.text = tr(GlobalVariables.JOY_BUTTONS.get(ProjectSettings.get_setting(active_action).values()[1][1].button_index));

func lose_focus(grid : Node) -> void:
	for child in grid.get_children():
		child.focus_mode = FOCUS_NONE;
		
func restore_focus(grid : Node) -> void:
	for child in grid.get_children():
		child.focus_mode = FOCUS_ALL;
		
#called when exiting the scene to update GlobalVariables
func update_global_variables() -> void:
	GlobalVariables.keyboard_controls	=	self.keyboard_controls;
	GlobalVariables.joypad_controls		=	self.joypad_controls;
#end

func action_exists(index : int) -> int:
	if ($KeyRemap/Tabs.current_tab == 0):
		var scancode = keyboard_controls[index];
		
		for code in keyboard_controls:
			if (code.index == index):
				continue;
			elif(code == scancode):
				return code.index;
	elif ($KeyRemap/Tabs.current_tab == 1):
		var button_index = joypad_controls[index];
		
		for code in joypad_controls:
			if (code.index == index):
				continue;
			elif (code == button_index):
				return code.index;
	
	return index;
#end
			

#Processes input events
func _input(event):
	if ($KeyChooser.visible): #only processed if the KeyChooser window is visible
		if ($KeyRemap/Tabs.current_tab == 0 && event is InputEventKey): #gets input from keyboard
			active_key = event.scancode;
			self.key_chooser_refresh();
			$KeyChooser.visible = false;
			$InputCooldown.start(0.1);
			self.lose_focus($KeyRemap/Tabs/MouseKeyboard);
		elif ($KeyRemap/Tabs.current_tab == 1 && event is InputEventJoypadButton): #gets input from joypad
			active_key = event.button_index;
			self.key_chooser_refresh();
			$KeyChooser.visible = false;
			$InputCooldown.start(0.1);
			self.lose_focus($KeyRemap/Tabs/Controller);
	else:	#if the player is not trying to choose a new key or button for an action
			#they can switch between MouseKeyboard and Controller tabs
		if ($KeyRemap/Tabs/Controller/BackButton.is_hovered() == false && $KeyRemap/Tabs/MouseKeyboard/BackButton.is_hovered() == false):
			if (event.is_action_pressed("right") && $KeyRemap/Tabs.current_tab == 0):
				$KeyRemap/Tabs.current_tab = 1;
				active_button = $KeyRemap/Tabs/Controller/LeftButton;
				active_button.grab_focus()
			
			if (event.is_action_pressed("left") && $KeyRemap/Tabs.current_tab == 1):
				$KeyRemap/Tabs.current_tab = 0;
				active_button = $KeyRemap/Tabs/MouseKeyboard/LeftButton;
				active_button.grab_focus()
				
			if (event.is_action_pressed("cancel")):
				self._on_BackButton_pressed();

func _on_KeyChooser_visibility_changed():
	if ($KeyChooser.visible == false):
		if ($KeyRemap/Tabs.current_tab == 0):
			ProjectSettings.get_setting(active_action).values()[1][0].scancode = active_key;
			self.keyboard_controls[active_property_index] = active_key;
		else:
			ProjectSettings.get_setting(active_action).values()[1][1].button_index = active_key;
			self.joypad_controls[active_property_index] = active_key;
		self.button_refresh();
		
		
func _on_InputCooldown_timeout():
	self.restore_focus($KeyRemap/Tabs/MouseKeyboard);
	self.restore_focus($KeyRemap/Tabs/Controller);
	active_button.grab_focus();

#############################

#############################
"""BUTTON PRESS PROCESSING"""
#############################

func _on_LeftButton_pressed():
	active_action = "input/left";
	active_property_index = GlobalVariables.Actions.LEFT;
	if ($KeyRemap/Tabs.current_tab == 0):
		active_key = ProjectSettings.get_setting(active_action).values()[1][0].scancode;
		active_button = $KeyRemap/Tabs/MouseKeyboard/LeftButton;
	elif ($KeyRemap/Tabs.current_tab == 1):
		active_key = ProjectSettings.get_setting(active_action).values()[1][1].button_index;
		active_button = $KeyRemap/Tabs/Controller/LeftButton;
	self.key_chooser_refresh();
	$KeyChooser.popup_centered();


func _on_RightButton_pressed():
	active_action = "input/right";
	active_property_index = GlobalVariables.Actions.RIGHT;
	if ($KeyRemap/Tabs.current_tab == 0):
		active_key = ProjectSettings.get_setting(active_action).values()[1][0].scancode;
		active_button = $KeyRemap/Tabs/MouseKeyboard/RightButton;
	elif ($KeyRemap/Tabs.current_tab == 1):
		active_key = ProjectSettings.get_setting(active_action).values()[1][1].button_index;
		active_button = $KeyRemap/Tabs/Controller/RightButton;
	self.key_chooser_refresh();
	$KeyChooser.popup_centered();


func _on_JumpButton_pressed():
	active_action = "input/jump";
	active_property_index = GlobalVariables.Actions.JUMP;
	if ($KeyRemap/Tabs.current_tab == 0):
		active_key = ProjectSettings.get_setting(active_action).values()[1][0].scancode;
		active_button = $KeyRemap/Tabs/MouseKeyboard/JumpButton;
	elif ($KeyRemap/Tabs.current_tab == 1):
		active_key = ProjectSettings.get_setting(active_action).values()[1][1].button_index;
		active_button = $KeyRemap/Tabs/Controller/JumpButton;
	self.key_chooser_refresh();
	$KeyChooser.popup_centered();


func _on_RunButton_pressed():
	active_action = "input/run";
	active_property_index = GlobalVariables.Actions.RUN;
	if ($KeyRemap/Tabs.current_tab == 0):
		active_key = ProjectSettings.get_setting(active_action).values()[1][0].scancode;
		active_button = $KeyRemap/Tabs/MouseKeyboard/RunButton;
	elif ($KeyRemap/Tabs.current_tab == 1):
		active_key = ProjectSettings.get_setting(active_action).values()[1][1].button_index;
		active_button = $KeyRemap/Tabs/Controller/RunButton;
	self.key_chooser_refresh();
	$KeyChooser.popup_centered();


func _on_ResetButton_pressed():
	active_action = "input/reset";
	active_property_index = GlobalVariables.Actions.RESET;
	if ($KeyRemap/Tabs.current_tab == 0):
		active_key = ProjectSettings.get_setting(active_action).values()[1][0].scancode;
		active_button = $KeyRemap/Tabs/MouseKeyboard/ResetButton;
	elif ($KeyRemap/Tabs.current_tab == 1):
		active_key = ProjectSettings.get_setting(active_action).values()[1][1].button_index;
		active_button = $KeyRemap/Tabs/Controller/ResetButton;
	self.key_chooser_refresh();
	$KeyChooser.popup_centered();


func _on_PauseButton_pressed():
	active_action = "input/pause";
	active_property_index = GlobalVariables.Actions.PAUSE;
	if ($KeyRemap/Tabs.current_tab == 0):
		active_key = ProjectSettings.get_setting(active_action).values()[1][0].scancode;
		active_button = $KeyRemap/Tabs/MouseKeyboard/PauseButton;
	elif ($KeyRemap/Tabs.current_tab == 1):
		active_key = ProjectSettings.get_setting(active_action).values()[1][1].button_index;
		active_button = $KeyRemap/Tabs/Controller/PauseButton;
	self.key_chooser_refresh();
	$KeyChooser.popup_centered();


func _on_TeleportButton_pressed():
	active_action = "input/teleport";
	active_property_index = GlobalVariables.Actions.TELEPORT;
	if ($KeyRemap/Tabs.current_tab == 0):
		active_key = ProjectSettings.get_setting(active_action).values()[1][0].scancode;
		active_button = $KeyRemap/Tabs/MouseKeyboard/TeleportButton;
	elif ($KeyRemap/Tabs.current_tab == 1):
		active_key = ProjectSettings.get_setting(active_action).values()[1][1].button_index;
		active_button = $KeyRemap/Tabs/Controller/TeleportButton;
	self.key_chooser_refresh();
	$KeyChooser.popup_centered();
		
func _on_BackButton_pressed():
	self.update_global_variables();
	self.visible = false;
	
func _on_DefaultButton_pressed():
	keyboard_controls	=	[ 65, 68, 32, 81, 82, 16777217, 16777237 ];
	joypad_controls		=	[ 14, 15, 0, 6, 3, 11, 7 ];
	
	ProjectSettings.get_setting("input/left").values()[1][0].scancode = keyboard_controls[GlobalVariables.Actions.LEFT];
	ProjectSettings.get_setting("input/right").values()[1][0].scancode = keyboard_controls[GlobalVariables.Actions.RIGHT];
	ProjectSettings.get_setting("input/jump").values()[1][0].scancode = keyboard_controls[GlobalVariables.Actions.JUMP];
	ProjectSettings.get_setting("input/reset").values()[1][0].scancode = keyboard_controls[GlobalVariables.Actions.RESET];
	ProjectSettings.get_setting("input/teleport").values()[1][0].scancode = keyboard_controls[GlobalVariables.Actions.TELEPORT];
	ProjectSettings.get_setting("input/pause").values()[1][0].scancode = keyboard_controls[GlobalVariables.Actions.PAUSE];
	ProjectSettings.get_setting("input/run").values()[1][0].scancode = keyboard_controls[GlobalVariables.Actions.RUN];
	
	ProjectSettings.get_setting("input/left").values()[1][1].button_index = joypad_controls[GlobalVariables.Actions.LEFT];
	ProjectSettings.get_setting("input/right").values()[1][1].button_index = joypad_controls[GlobalVariables.Actions.RIGHT];
	ProjectSettings.get_setting("input/jump").values()[1][1].button_index = joypad_controls[GlobalVariables.Actions.JUMP];
	ProjectSettings.get_setting("input/teleport").values()[1][1].button_index = joypad_controls[GlobalVariables.Actions.TELEPORT];
	ProjectSettings.get_setting("input/pause").values()[1][1].button_index = joypad_controls[GlobalVariables.Actions.PAUSE];
	ProjectSettings.get_setting("input/reset").values()[1][1].button_index = joypad_controls[GlobalVariables.Actions.RESET];
	ProjectSettings.get_setting("input/run").values()[1][1].button_index = joypad_controls[GlobalVariables.Actions.RUN];
	
	self.draw_buttons();
	
###########################
