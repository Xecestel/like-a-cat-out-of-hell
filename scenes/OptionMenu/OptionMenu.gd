extends Control

#constants#
const TITLE_ID			=	"OPTIONS";
const MUTE_ID			=	"MUTE";
const WINDOW_ID			=	"WINDOW_MODE";
const LANGUAGE_ID		= "LANGUAGE";
const LANGUAGES			= ["EN", "IT"];
const WINDOW_MODES		= ["WINDOW_WINDOWED", "WINDOW_BORDERLESS", "WINDOW_FULLSCREEN"];
const RESET_ID			= "RESET";
const ACCEPT_ID			= "ACCEPT";
const CANCEL_ID			= "CANCEL";
const YES_ID			= "YES";
const NO_ID				= "NO";
const CHANGE_LOST_ID	= "CHANGES_LOST";
const RESET_DEFAULT_ID	= "RESET_DEFAULT";
const ACCEPT_CHANGES_ID	= "ACCEPT_CHANGES";
const CONFIRM_TITLE		= "CONFIRM";

const WINDOW_MODE_DISPLAY	= "WINDOW_MODE_DISPLAY";
const LANGUAGE_DISPLAY		= "LANGUAGE_DISPLAY";
const VSYNC_DISPLAY			= "VSYNC_DISPLAY";
const MUTE_DISPLAY			= "MUTE_DISPLAY";
const SFX_DISPLAY			= "SFX_DISPLAY";
const BGM_DISPLAY			= "BGM_DISPLAY";
const CONTROLS_DISPLAY		= "CONTROLS_DISPLAY";
const CURRENT_VALUE_ID		= "CURRENT_VALUE";
###########

#variables#

#Global Variables#
var active_button;
var description_panel;
var display_text;
var display_value = 0;
##################


#Audio Options tab variables#
var bgm_level			=	80;
var sfx_level			=	80;
var mute				=	false;
var language_id			=	0;
var window_mode_id		=	2;
var vsync_enabled		=	true;
############################

#Game Options tab variables#

##########################

#booleans for confirmation dialogue#
var resetting			=	false;
var accepting			=	false;
var canceling			=	false;
###################################

##########

func _ready():
	self.localization();
	
	description_panel	=	$MarginContainer/OptionsWindow/DescriptionPanel;
	
	active_button = $MarginContainer/OptionsWindow/AudioGrid/BGMSlider;
	
	self.reset_to_saved();
	
	if (mute):
		active_button = $MarginContainer/OptionsWindow/AudioGrid/MuteButton;
	else:
		active_button		=	$MarginContainer/OptionsWindow/AudioGrid/BGMSlider;
		
	active_button.grab_focus();

func _process(delta):
	if ($MarginContainer/OptionsWindow/AudioGrid/WindowModeButton.is_hovered() == true):
		$MarginContainer/OptionsWindow/AudioGrid/WindowModeButton.grab_focus();
		active_button = $MarginContainer/OptionsWindow/AudioGrid/WindowModeButton;
		display_text = tr(WINDOW_MODE_DISPLAY);
		display_value = -1;
	
	if ($MarginContainer/OptionsWindow/AudioGrid/VSyncButton.is_hovered() == true):
		$MarginContainer/OptionsWindow/AudioGrid/VSyncButton.grab_focus();
		active_button = $MarginContainer/OptionsWindow/AudioGrid/VSyncButton;
		display_text = tr(VSYNC_DISPLAY);
		display_value = -1;
		
	if ($MarginContainer/OptionsWindow/ControlButtons/ResetButton.is_hovered() == true):
		$MarginContainer/OptionsWindow/ControlButtons/ResetButton.grab_focus();
		active_button = null;
	
	if ($MarginContainer/OptionsWindow/ControlButtons/AcceptButton.is_hovered() == true):
		$MarginContainer/OptionsWindow/ControlButtons/AcceptButton.grab_focus();
		active_button = null;
	
	if ($MarginContainer/OptionsWindow/ControlButtons/CancelButton.is_hovered() == true):
		$MarginContainer/OptionsWindow/ControlButtons/CancelButton.grab_focus();
		active_button = null;
	
	if ($MarginContainer/OptionsWindow/AudioGrid/MuteButton.is_hovered() == true):
		$MarginContainer/OptionsWindow/AudioGrid/MuteButton.grab_focus();
		active_button = $MarginContainer/OptionsWindow/AudioGrid/MuteButton;
		display_text = tr(MUTE_DISPLAY);
		display_value = -1;
		
	if ($MarginContainer/OptionsWindow/AudioGrid/LanguageButton.is_hovered() == true):
		$MarginContainer/OptionsWindow/AudioGrid/LanguageButton.grab_focus();
		active_button = $MarginContainer/OptionsWindow/AudioGrid/LanguageButton;
		display_text = tr(LANGUAGE_DISPLAY);
		display_value = -1;
	
	if ($MarginContainer/OptionsWindow/AudioGrid/ControlsButton.is_hovered() == true):
		$MarginContainer/OptionsWindow/AudioGrid/ControlsButton.grab_focus();
		active_button = $MarginContainer/OptionsWindow/AudioGrid/ControlsButton;
		display_text = tr(CONTROLS_DISPLAY);
		display_value = -1;
		
	if(active_button != null):
		var h_margin = 20;
		description_panel.rect_position.y = active_button.rect_position.y + active_button.get_parent().rect_position.y;
		description_panel.rect_position.x = active_button.rect_position.x + active_button.get_parent().rect_position.x - (description_panel.rect_size.x + h_margin);
		description_panel.visible = true;
		draw_panel(display_text, display_value);
	else:
		description_panel.visible = false;
		
	
	#if cancel button is pressed on confirmation dialog
	#resets boolean switches
	if ($MarginContainer/OptionsWindow/ConfirmationDialog.visible):
		if($MarginContainer/OptionsWindow/ConfirmationDialog.get_cancel().pressed):
			if(self.accepting):
				self.accepting					=	false;
				
			if(self.canceling):
				self.canceling					=	false;
			
			if (self.resetting):
				self.resetting				=	false;
#end

func localization() -> void:
	$MarginContainer/OptionsWindow/Title.text						= tr(TITLE_ID);
	$MarginContainer/OptionsWindow/AudioGrid/MuteLabel.text			= tr(MUTE_ID);
	$MarginContainer/OptionsWindow/AudioGrid/WindowModeLabel.text	= tr(WINDOW_ID);
	$MarginContainer/OptionsWindow/AudioGrid/LanguageLabel.text		= tr(LANGUAGE_ID);
	$MarginContainer/OptionsWindow/ControlButtons/ResetButton.text	= tr(RESET_ID);
	$MarginContainer/OptionsWindow/ControlButtons/AcceptButton.text	= tr(ACCEPT_ID);
	$MarginContainer/OptionsWindow/ControlButtons/CancelButton.text	= tr(CANCEL_ID);

	$MarginContainer/OptionsWindow/AudioGrid/WindowModeButton	.clear();	
	$MarginContainer/OptionsWindow/AudioGrid/WindowModeButton	.add_item(tr(WINDOW_MODES[0]), 0);
	$MarginContainer/OptionsWindow/AudioGrid/WindowModeButton	.add_item(tr(WINDOW_MODES[1]), 1);
	$MarginContainer/OptionsWindow/AudioGrid/WindowModeButton	.add_item(tr(WINDOW_MODES[2]), 2);
	$MarginContainer/OptionsWindow/AudioGrid/WindowModeButton	.select(self.window_mode_id);
	
	$MarginContainer/OptionsWindow/AudioGrid/LanguageButton		.clear();
	$MarginContainer/OptionsWindow/AudioGrid/LanguageButton		.add_item(tr(LANGUAGES[0]), 0);
	$MarginContainer/OptionsWindow/AudioGrid/LanguageButton		.add_item(tr(LANGUAGES[1]), 1)
	$MarginContainer/OptionsWindow/AudioGrid/LanguageButton		.select(self.language_id);
	
	$MarginContainer/OptionsWindow/ConfirmationDialog.get_ok().text = tr(YES_ID);
	$MarginContainer/OptionsWindow/ConfirmationDialog.get_cancel().text = tr(NO_ID);
	
	$MarginContainer/OptionsWindow/ConfirmationDialog.window_title = tr(CONFIRM_TITLE);
#end
	
func draw_panel(text, current_value):
	description_panel.rect_size = Vector2(384, 95);
	if(text != null):
		$MarginContainer/OptionsWindow/DescriptionPanel/VBoxContainer/Text.text = text;
	if (current_value >= 0):
		$MarginContainer/OptionsWindow/DescriptionPanel/VBoxContainer/CurrentValue.text = tr(CURRENT_VALUE_ID) + ": " + str(current_value) + "%";
		$MarginContainer/OptionsWindow/DescriptionPanel/VBoxContainer/CurrentValue.visible = true;
	else:
		$MarginContainer/OptionsWindow/DescriptionPanel/VBoxContainer/CurrentValue.visible = false;
	pass
	
func lose_focus(grid : Node) -> void:
	for child in grid.get_children():
		child.focus_mode = FOCUS_NONE;
		
func restore_focus(grid : Node) -> void:
	for child in grid.get_children():
		child.focus_mode = FOCUS_ALL;

######################################


######################################
" GAME OPTIONS PROCESSING FUNCTIONS "
######################################

func _on_WindowModeButton_item_selected(ID):
	self.window_mode_id = ID;
	
	#changes window mode
	if (self.window_mode_id == 0): #if fullscreen selected
		OS.window_borderless = false;
		OS.window_fullscreen = false;
		
	if (self.window_mode_id == 1): #if borderless selected
		OS.window_fullscreen = false;
		OS.window_borderless = true;

	if (self.window_mode_id == 2): #if window mode selected
		OS.window_borderless = false;
		OS.window_fullscreen = true;
		

#button_pressed is a boolean
func _on_VSyncButton_toggled(button_pressed):
	self.vsync_enabled = button_pressed; #on/off vsync
	OS.vsync_enabled = self.vsync_enabled;
	pass

func _on_BGMSlider_focus_entered():
	display_value = $MarginContainer/OptionsWindow/AudioGrid/BGMSlider.value;
	if (!mute):
		active_button = $MarginContainer/OptionsWindow/AudioGrid/BGMSlider;
		display_text = tr(BGM_DISPLAY);
	else:
		active_button = null;


func _on_SFXSlider_focus_entered():
	display_value = $MarginContainer/OptionsWindow/AudioGrid/SFXSlider.value;
	if (!mute):
		active_button = $MarginContainer/OptionsWindow/AudioGrid/SFXSlider;
		display_text = tr(SFX_DISPLAY);
	else:
		active_button = null;


#sets the bgm level
#value is a float between 0.0 and 100.0
func _on_BGMSlider_value_changed(value):
	self.bgm_level = value;
	display_value = bgm_level;
	
	if (bgm_level == 0):
		AudioServer.set_bus_mute(AudioServer.get_bus_index("BGM"), true);
		AudioServer.set_bus_mute(AudioServer.get_bus_index("SoundTrack"), true);
	else:	
		var bgm_db = (bgm_level * 104 / 100) - 80;
		AudioServer.set_bus_mute(AudioServer.get_bus_index("BGM"), false);
		AudioServer.set_bus_mute(AudioServer.get_bus_index("SoundTrack"), false);
		AudioServer.set_bus_volume_db(AudioServer.get_bus_index("BGM"), bgm_db);
		AudioServer.set_bus_volume_db(AudioServer.get_bus_index("SoundTrack"), bgm_db);

func _on_SFXSlider_value_changed(value):
	self.sfx_level = value;		
	display_value = sfx_level;
	
	if (sfx_level == 0):
		AudioServer.set_bus_mute(AudioServer.get_bus_index("SFX"), true);
	else:
		var sfx_db = (sfx_level * 104 / 100) - 80;
		AudioServer.set_bus_mute(AudioServer.get_bus_index("SFX"), false);
		AudioServer.set_bus_volume_db(AudioServer.get_bus_index("SFX"), sfx_db);

#mutes/unmutes the game
#button_pressed is a boolean
func _on_MuteButton_toggled(button_pressed):
	mute = button_pressed;
	if (mute):
		$MarginContainer/OptionsWindow/AudioGrid/BGMSlider.editable = false;
		$MarginContainer/OptionsWindow/AudioGrid/SFXSlider.editable = false;
	else:
		$MarginContainer/OptionsWindow/AudioGrid/BGMSlider.editable = true;
		$MarginContainer/OptionsWindow/AudioGrid/SFXSlider.editable = true;
		
	AudioServer.set_bus_mute(AudioServer.get_bus_index("Master"), mute);


func _on_LanguageButton_item_selected(ID):
	self.language_id = ID;
	TranslationServer.set_locale(GlobalVariables.LANGUAGES[ID]);
	self.localization();
	
func _on_ControlsButton_pressed():
	self.lose_focus($MarginContainer/OptionsWindow/AudioGrid);
	self.lose_focus($MarginContainer/OptionsWindow/ControlButtons);
	$KeyRemap.visible = true;
	$KeyRemap.grab_focus();
	self.focus_mode = Control.FOCUS_NONE;
	
func _on_KeyRemap_visibility_changed():
	if ($KeyRemap.visible == false):
		self.restore_focus($MarginContainer/OptionsWindow/AudioGrid);
		self.restore_focus($MarginContainer/OptionsWindow/ControlButtons);
		$MarginContainer/OptionsWindow/AudioGrid/BGMSlider.grab_focus();

	
################################################
	
	
################################################
" RESET ACCEPT AND CANCEL PROCESSING FUNCTIONS "
################################################


func _on_ResetButton_pressed():
	$MarginContainer/OptionsWindow/ConfirmationDialog.dialog_text = tr(RESET_DEFAULT_ID);
	$MarginContainer/OptionsWindow/ConfirmationDialog.popup_centered();
	self.resetting = true;
	pass

#resets buttons and switches to default
func reset_to_default():
	$MarginContainer/OptionsWindow/AudioGrid/WindowModeButton.selected	=	0;
	self._on_WindowModeButton_item_selected(0);
	
	
	$MarginContainer/OptionsWindow/AudioGrid/VSyncButton.pressed		=	true;
	self._on_VSyncButton_toggled(true);
	
	$MarginContainer/OptionsWindow/AudioGrid/BGMSlider.value = 80;
	$MarginContainer/OptionsWindow/AudioGrid/SFXSlider.value = 80;
	
	$MarginContainer/OptionsWindow/AudioGrid/MuteButton.pressed = false;
	self._on_MuteButton_toggled(false);
	
	$MarginContainer/OptionsWindow/AudioGrid/LanguageButton.selected = 0;
	self._on_LanguageButton_item_selected(0);
#end

func _on_AcceptButton_pressed():
	$MarginContainer/OptionsWindow/ConfirmationDialog.dialog_text = tr(ACCEPT_CHANGES_ID);
	$MarginContainer/OptionsWindow/ConfirmationDialog.popup_centered();
	self.accepting = true;
	pass

#updates global variables
func update_global_variables():
	#changes window mode		
	GlobalVariables.window_mode_id = self.window_mode_id;
			
	#toggles vsync
	GlobalVariables.vsync_enabled = self.vsync_enabled;

	#updates sound and music values
	GlobalVariables.mute = self.mute;	
	GlobalVariables.bgm_level = self.bgm_level;	
	GlobalVariables.sfx_level = self.sfx_level;
	
	#localization
	GlobalVariables.language_id = self.language_id;
#end

func _on_CancelButton_pressed():
	$MarginContainer/OptionsWindow/ConfirmationDialog.dialog_text = tr(CHANGE_LOST_ID);
	$MarginContainer/OptionsWindow/ConfirmationDialog.popup_centered();
	self.canceling = true;
#end

#resets buttons to saved variables
func reset_to_saved():
	$MarginContainer/OptionsWindow/AudioGrid/BGMSlider.value = GlobalVariables.bgm_level;
	$MarginContainer/OptionsWindow/AudioGrid/SFXSlider.value = GlobalVariables.sfx_level;
	
	#if master volume is mute, press the mute button
	self._on_MuteButton_toggled(AudioServer.is_bus_mute(AudioServer.get_bus_index("Master")));
	$MarginContainer/OptionsWindow/AudioGrid/MuteButton.pressed = GlobalVariables.mute;

		
	#if vsync is enabled, press the vsync button
	self._on_VSyncButton_toggled(GlobalVariables.vsync_enabled);
	$MarginContainer/OptionsWindow/AudioGrid/VSyncButton.pressed = GlobalVariables.vsync_enabled;

	#updates window mode id
	self._on_WindowModeButton_item_selected(GlobalVariables.window_mode_id);
	$MarginContainer/OptionsWindow/AudioGrid/WindowModeButton.select(GlobalVariables.window_mode_id);
	
	#updates language selection
	self._on_LanguageButton_item_selected(GlobalVariables.language_id);
	$MarginContainer/OptionsWindow/AudioGrid/LanguageButton.select(GlobalVariables.language_id);
#end


func _on_ConfirmationDialog_confirmed():
	if (accepting):
		self.update_global_variables();
		SaveLoad.save_game();
		transitionMgr.transitionTo("res://scenes/title/title.tscn", 2.0, false);
		accepting = false;
		
	elif (resetting):
		self.reset_to_default();
		resetting = false;
		
	elif (canceling):
		self.reset_to_saved();
		transitionMgr.transitionTo("res://scenes/title/title.tscn", 2.0, false);
		canceling = false;
#end

########################################