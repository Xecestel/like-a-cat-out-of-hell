extends TextureButton

#variables#
export(String) var id_name;

#signals#
signal being_pressed;

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Item_pressed():
	emit_signal("being_pressed", self);
