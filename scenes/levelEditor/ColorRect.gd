extends NinePatchRect

func _ready():
	self.self_modulate = Color(1, 1, 1, 0);

func _on_ColorRect_mouse_entered():
	self.self_modulate = Color(1, 1, 1, 1);
	self.get_parent().get_parent().active_rect = self;


func _on_ColorRect_mouse_exited():
	self.self_modulate = Color(1, 1, 1, 0);
